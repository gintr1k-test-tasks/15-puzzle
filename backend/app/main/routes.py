from flask import jsonify, request
from datetime import datetime
import json
from app import db
from app.main import bp
from app.models import Game


@bp.route('/game/classic', methods=['POST'])
def create_classic_game():
    game = Game(table_size=4)
    game.initialize_classic_table()

    db.session.add(game)
    db.session.commit()

    return jsonify(game.to_dict())


@bp.route('/game/random', methods=['POST'])
def create_random_game():
    game = Game(table_size=4)
    game.initialize_random_table()

    db.session.add(game)
    db.session.commit()

    return jsonify(game.to_dict())


@bp.route('/game', methods=['GET'])
def get_game():
    id = request.args.get('id', None, type=int)
    game = Game.query.filter_by(id=id).first_or_404()

    return jsonify(game.to_dict())


@bp.route('/game', methods=['POST'])
def edit_game():
    id = request.form.get('id')
    pos_col = request.form.get('col')
    pos_row = request.form.get('row')

    if not pos_col or not pos_row or not id:
        return jsonify({'success': False})

    try:
        id = str(float(id))

        pos_col = int(float(pos_col))
        pos_row = int(float(pos_row))
    except Exception as error:
        return jsonify({'success': False})

    def check(pos_col, pos_row, table):
        if pos_col >= 0 and len(table) - 1 >= pos_col \
                and pos_row >= 0 and len(table[pos_col]) - 1 >= pos_row \
                and table[pos_col][pos_row] is None:
            return True

    game = Game.query.filter_by(id=id).first_or_404()
    table = json.loads(game.table)
    if check(pos_col, pos_row - 1, table):
        table[pos_col][pos_row - 1] = table[pos_col][pos_row]
        table[pos_col][pos_row] = None

    elif check(pos_col, pos_row + 1, table):
        table[pos_col][pos_row + 1] = table[pos_col][pos_row]
        table[pos_col][pos_row] = None

    elif check(pos_col - 1, pos_row, table):
        table[pos_col - 1][pos_row] = table[pos_col][pos_row]
        table[pos_col][pos_row] = None

    elif check(pos_col + 1, pos_row, table):
        table[pos_col + 1][pos_row] = table[pos_col][pos_row]
        table[pos_col][pos_row] = None

    else:
        return jsonify({'success': False})

    game.table = json.dumps(table)
    game.last_save_at = datetime.utcnow()

    db.session.commit()

    return jsonify(game.to_dict())
