# 15 Puzzle Documentation

## Sections:
- [Installation](#installation)
- [API](#api)

## Installation:
Clone repository first:
```
git clone https://github.com/GinTR1k/15-puzzle.git
```
or
```
git clone https://gitlab.com/GinTR1k/15-puzzle.git
```

Then, make virtual env and activate it:
```
python -m venv venv && source/bin/activate
```

Install requirements:
```
pip install -r requirements.txt
```

Create the `.env` file:
```
echo > .env
```

Fill the `.env file`:
```
FLASK_DEBUG=true
FLASK_APP=15-puzzle.py
SECRET_KEY=YOUR-SECRET-KEY
DATABASE_URL=YOUR-DATABASE-URI # For example: postgresql://username:password@localhost/database
```

Run migrations:
```
flask db upgrade
```

Run the app:
```
flask run
```

## API
- [Objects](#objects)
- [Routes](#routes)

### Objects:
- [Game](#object-game)

### Object Game
Field | Data type | Notes
-- | -- | --
`id`  | Integer | Increment id
`created_at`  | DateTime |  _UTC timezone_. Timestamp of creating object.
`last_save_at`  | DateTime |  _UTC timezone_. Timestamp of last changes.
`table_size`  | Integer |  **Default: 4**. A size of the game's table. Returns 64bit integer, which means numeric of columns and rows.
`table`  | JSON String |  List of rows table. Contains numbers and None-type of empty block.


### Routes
Method | URL | Parameters | Notes
-- | -- | -- | --
POST  | `/game/classic` | None |  Create new game instance with Classic table.
POST  | `/game/random` | None |  Create new game instance with Random table.
GET  | `/game` | `id` - Integer |  Get the instance game by `id`. If game not found, returns 404 status code.
POST  | `/game` | `id`, `col`, `row` - Integer |  Move the `col`:`row` position to empty block in game with id=`id`, which must be on `row`+1 or `row`-1 or `col`+1 or `col`-1. If empty block on specified positions not found, returns name `success` with `False` value.

\* On all success results returns Game instance.
