import { Game } from './game';

describe('Game', () => {
  it('should create an instance', () => {
    expect(new Game()).toBeTruthy();
  });

  it('should accept values in the constructor', () => {
      let game = new Game({
        table_size: 4
      });
      expect(game.table_size).toEqual(4);
    });
  });
