export class Game {
  id: number;
  created_at: string;
  last_save_at: string;
  table: number;
  table_size: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
